<?php
/**
 * @file
 * Zxcvbn constraint for Password Policy module.
 */

$plugin = array(
  'admin form callback' => 'password_policy_zxcvbn_admin_form',
  'constraint callback' =>  'password_policy_zxcvbn_constraint',
  'message' => array(
    t('Password must meet or exceed a level of strength based on its entropy.'),
    t('A strong password will not be comprised of common dictionary words (like "password"), character sequences, repeating characters, or common dates and times.')),
  'prime value' => 'zxcvbn',
  'config' => array(
    'zxcvbn' => NULL,
  ),
);

/**
 * Admin form callback for zxcvbn constraint.
 */
function password_policy_zxcvbn_admin_form($form, &$form_state, $constraint) {
  $sub_form['zxcvbn_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Zxcvbn score'),
  );
  $enabled = TRUE;
  //if (!class_exists('Zxcvbn')) {
    //$enabled = FALSE;
    $sub_form['zxcvbn_fieldset']['disclaimer'] = array(
      '#markup' => t('Zxcvbn-PHP library is not available. Install the Zxcvbn Lib module to use this constraint.'),
    );
  //}
  $sub_form['zxcvbn_fieldset']['zxcvbn'] = array(
    '#type' => 'textfield',
    '#title' => t('Score'),
    '#default_value' => $constraint->config['zxcvbn'],
    '#description' => t('Password will be required to meet or exceed the strength score measured by Zxcvbn-PHP (0-4).'),
    '#access' => $enabled,
  );

  return $sub_form;
}

/**
 * Check score against Zxcvbn-PHP
 */
function password_policy_zxcvbn_constraint($password, $account, $constraint) {
  $zxcvbn = new ZxcvbnPhp\Zxcvbn();
  $strength = $zxcvbn->passwordStrength($password);
  return $strength['score'] >= $constraint->config['zxcvbn'];
}
