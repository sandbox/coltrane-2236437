Clone, copy, or download the Zxcvbn-PHP project from
https://github.com/bjeavons/zxcvbn-php to the lib directory in
zxcvbn_lib module.

Expected file structure on successful installation:

/INSTALL.txt
/password_policy_zxcvbn.info
/password_policy_zxcvbn.module
/lib/zxcvbn-php
  -- /src/ZxcvbnPhp/Zxcvbn.php 
